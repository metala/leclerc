package jsonrpc

import (
	"encoding/json"
)

const Version = "2.0"

type ErrorCode int

const (
	errcodeParse         ErrorCode = -32700
	errcodeInvalidReq    ErrorCode = -32600
	errcodeNoMethod      ErrorCode = -32601
	errcodeInvalidParams ErrorCode = -32602
	errcodeInternalError ErrorCode = -32603
	errcodeServerError   ErrorCode = -32000
)

var (
	ErrParseError     = &Error{Code: errcodeParse, Message: "Parse error"}
	ErrMethodNotFound = &Error{Code: errcodeNoMethod, Message: "Method not found"}
	ErrInvalidParams  = &Error{Code: errcodeInvalidParams, Message: "Invalid params"}
	ErrInternal       = &Error{Code: errcodeInternalError, Message: "Internal error"}
)

type Request struct {
	Version string            `json:"jsonrpc"`
	Id      json.RawMessage   `json:"id"`
	Method  string            `json:"method"`
	Params  []json.RawMessage `json:"params"`
}

type Response struct {
	Version string          `json:"jsonrpc"`
	Id      json.RawMessage `json:"id"`
	Result  json.RawMessage `json:"result,omitempty"`
	Error   *Error          `json:"error,omitempty"`
}

type Error struct {
	Code    ErrorCode   `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func (e *Error) Error() string {
	return e.Message
}

func NewInternalError(err error) *Error {
	return &Error{Code: errcodeInternalError, Message: err.Error()}
}

package ethsvc

import (
	"github.com/ledgerwatch/erigon-lib/kv"
)

type EthService struct {
	db kv.RoDB
}

func New(db kv.RoDB) *EthService {
	return &EthService{
		db: db,
	}
}

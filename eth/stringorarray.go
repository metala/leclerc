package ethsvc

import (
	"encoding/json"

	"github.com/ethereum/go-ethereum/common"
	"go.metala.org/leclerc/jsonrpc"
)

type singleOrArrayAddress []common.Address

func (s *singleOrArrayAddress) UnmarshalJSON(b []byte) error {
	if len(b) == 0 {
		return jsonrpc.ErrInvalidParams
	}

	switch b[0] {
	case '"':
		var str common.Address
		if err := json.Unmarshal(b, &str); err != nil {
			return err
		}
		*s = []common.Address{str}
	case '[':
		var array []common.Address
		if err := json.Unmarshal(b, &array); err != nil {
			return err
		}
		*s = array
	default:
		return jsonrpc.ErrInvalidParams
	}

	return nil
}

type singleOrArrayHash []common.Hash

func (s *singleOrArrayHash) UnmarshalJSON(b []byte) error {
	if len(b) == 0 {
		return jsonrpc.ErrInvalidParams
	}

	switch b[0] {
	case '"':
		var str common.Hash
		if err := json.Unmarshal(b, &str); err != nil {
			return err
		}
		*s = []common.Hash{str}
	case '[':
		var array []common.Hash
		if err := json.Unmarshal(b, &array); err != nil {
			return err
		}
		*s = array
	default:
		return jsonrpc.ErrInvalidParams
	}

	return nil
}

package leclerc

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/ledgerwatch/erigon-lib/kv"
	"github.com/ledgerwatch/erigon-lib/kv/mdbx"
	lwlog "github.com/ledgerwatch/log/v3"

	ethsvc "go.metala.org/leclerc/eth"
)

type LeClerc struct {
	db     kv.RoDB
	logger *log.Logger

	// services
	eth *ethsvc.EthService

	// stats
	requestID uint64
}

func New(path string, concurrency int) (*LeClerc, error) {
	logger := log.New(os.Stderr, "", log.Ldate|log.Ltime)
	mdbxLogger := lwlog.New()
	limiter := make(chan struct{}, concurrency)
	kvDb, err := mdbx.NewMDBX(mdbxLogger).
		RoTxsLimiter(limiter).
		Path(path).
		Readonly().
		Open()
	if err != nil {
		return nil, fmt.Errorf("failed to open MDBX: %w", err)
	}

	return &LeClerc{
		db:     kvDb,
		logger: logger,
		eth:    ethsvc.New(kvDb),
	}, nil
}

func (l *LeClerc) Run(bind string) {
	s := &http.Server{
		Addr:           bind,
		Handler:        l,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 4 << 10,
	}

	if err := s.ListenAndServe(); err != nil {
		log.Fatalf("Error in ListenAndServe: %s", err)
	}
}

func (l *LeClerc) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer recover()

	l.requestID += 1

	l.logger.Printf("#%d | %s %s | peer=%s, agent=%s",
		l.requestID,
		r.Method,
		r.RequestURI,
		r.RemoteAddr,
		r.UserAgent(),
	)

	begin := time.Now()
	l.rpcHandler(w, r, int(l.requestID))
	end := time.Now()

	l.logger.Printf("#%d | finished in %v",
		l.requestID,
		end.Sub(begin),
	)
}

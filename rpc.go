package leclerc

import (
	"bufio"
	"context"
	"encoding/json"
	"log"
	"net"
	"net/http"

	"go.metala.org/leclerc/jsonrpc"
)

type serviceMethod func(context.Context, []json.RawMessage, *jsonrpc.ResponseWriter) error

func (l *LeClerc) getMethod(method string) serviceMethod {
	switch method {
	case "eth_getLogs":
		return l.eth.GetLogs
	default:
		return nil
	}
}

func (l *LeClerc) rpcHandler(w http.ResponseWriter, r *http.Request, requestID int) {
	w.Header().Set("Content-Type", "application/json")
	w.Header()["Date"] = nil

	dec := json.NewDecoder(r.Body)
	var req jsonrpc.Request
	if err := dec.Decode(&req); err != nil {
		r.Body.Close()
		w.WriteHeader(400)
		resp, _ := json.Marshal(jsonrpc.Response{
			Version: jsonrpc.Version,
			Error:   jsonrpc.ErrParseError,
		})
		w.Write(resp)
		return
	}
	r.Body.Close()

	method := l.getMethod(req.Method)
	if method == nil {
		w.WriteHeader(400)
		resp, _ := json.Marshal(jsonrpc.Response{
			Version: jsonrpc.Version,
			Error:   jsonrpc.ErrMethodNotFound,
		})
		w.Write(resp)
		return
	}

	paramsJSON, _ := json.Marshal(req.Params)
	log.Printf("#%d | %s() | params=%s", requestID, req.Method, paramsJSON)

	wr := makeHTTPCompressor(w, r)
	w.WriteHeader(http.StatusOK)

	bw := bufio.NewWriterSize(wr, 16<<10)
	jw := jsonrpc.NewResponseWriter(bw)
	c := context.Background()
	err := l.executeMethod(c, method, &req, jw)
	if err != nil {
		// close on error while streaming
		hj, ok := w.(http.Hijacker)
		if !ok {
			return
		}
		conn, _, _ := hj.Hijack()
		if conn != nil {
			// terminate abruptly with RST
			if tconn, ok := conn.(*net.TCPConn); ok {
				tconn.SetLinger(0)
			}
			conn.Close()
		}
	}
}

func (l *LeClerc) executeMethod(c context.Context, method serviceMethod, req *jsonrpc.Request, w *jsonrpc.ResponseWriter) error {
	w.WriteString(`{"jsonrpc":"2.0","id":`)
	w.Write(rawIdAsBytes(req.Id))
	w.WriteByte(',')

	err := method(c, req.Params, w)
	if err != nil {
		if e, ok := err.(*jsonrpc.Error); ok {
			w.WriteRpcError(e)
			w.WriteByte('}')
			return nil
		}
		return err
	}
	w.WriteByte('}')

	return nil
}

func rawIdAsBytes(raw json.RawMessage) []byte {
	if raw == nil {
		return []byte("null")
	}
	return raw
}

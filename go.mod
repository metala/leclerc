module go.metala.org/leclerc

go 1.17

require (
	github.com/RoaringBitmap/roaring v0.9.4
	github.com/dchest/cbrotli v1.0.9
	github.com/ethereum/go-ethereum v1.10.16
	github.com/ledgerwatch/erigon v1.9.7-0.20220315173810-25a68e08a528
	github.com/ledgerwatch/erigon-lib v0.0.0-20220321072621-26a8c3c571fa
	github.com/ledgerwatch/log/v3 v3.4.1
	github.com/spf13/pflag v1.0.5
)

require (
	github.com/StackExchange/wmi v0.0.0-20180116203802-5d049714c4a6 // indirect
	github.com/VictoriaMetrics/metrics v1.18.1 // indirect
	github.com/bits-and-blooms/bitset v1.2.0 // indirect
	github.com/btcsuite/btcd v0.21.0-beta // indirect
	github.com/c2h5oh/datasize v0.0.0-20200825124411-48ed595a09d2 // indirect
	github.com/deckarep/golang-set v1.8.0 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/holiman/uint256 v1.2.0 // indirect
	github.com/mattn/go-colorable v0.1.11 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mschoch/smat v0.2.0 // indirect
	github.com/shirou/gopsutil v3.21.4-0.20210419000835-c7a38de76ee5+incompatible // indirect
	github.com/tklauser/go-sysconf v0.3.9 // indirect
	github.com/tklauser/numcpus v0.3.0 // indirect
	github.com/torquem-ch/mdbx-go v0.22.18 // indirect
	github.com/ugorji/go/codec v1.1.13 // indirect
	github.com/valyala/fastrand v1.1.0 // indirect
	github.com/valyala/histogram v1.2.0 // indirect
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/natefinch/npipe.v2 v2.0.0-20160621034901-c1b8fa8bdcce // indirect
)

package main

import (
	"fmt"
	"os"

	flag "github.com/spf13/pflag"
	"go.metala.org/leclerc"
)

var version = "undef"

func showSplash() {
	fmt.Printf(`
############################
#    It is I, LeClerc!     #
#                          #
#        /%5s/           #
############################

`, version)
}

func main() {
	var dbPath string
	var bindAddress string
	var concurrency int

	flag.StringVarP(&dbPath, "db-path", "p", "", "the path to Erigon chaindata (required)")
	flag.StringVarP(&bindAddress, "bind-address", "b", ":10312", "the address and port to bind the http server")
	flag.IntVarP(&concurrency, "concurrency", "c", 1, "database transactions concurrency")
	flag.Parse()

	if dbPath == "" {
		flag.Usage()
		os.Exit(1)
	}

	showSplash()
	fmt.Println("Settings:")
	fmt.Println("Database path =", dbPath)
	fmt.Println("Tx concurrency =", concurrency)
	fmt.Println()
	fmt.Println("Opening Erigon database...")
	svc, err := leclerc.New(dbPath, concurrency)
	if err != nil {
		println(err)
		os.Exit(2)
	}

	fmt.Printf("Starting HTTP server on %s...\n", bindAddress)
	svc.Run(bindAddress)
}

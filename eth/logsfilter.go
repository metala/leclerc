package ethsvc

import (
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
)

type logsFilter struct {
	addresses []common.Address
	topics    [][]common.Hash
}

func newFilter(addresses []common.Address, topics [][]common.Hash) *logsFilter {
	f := &logsFilter{
		addresses: addresses,
		topics:    topics,
	}
	if f.addresses == nil {
		f.addresses = []common.Address{}
	}

	if f.topics == nil {
		f.topics = [][]common.Hash{}
	}
	for i := len(f.topics); i < 4; i++ {
		f.topics = append(f.topics, []common.Hash{})
	}

	return f
}

func (f *logsFilter) hasAddress(address common.Address) bool {
	if len(f.addresses) == 0 {
		return true
	}
	for _, a := range f.addresses {
		if address == a {
			return true
		}
	}
	return false
}
func (f *logsFilter) hasTopic(idx int, topic common.Hash) bool {
	if len(f.topics[idx]) == 0 {
		return true
	}
	for _, t := range f.topics[idx] {
		if topic == t {
			return true
		}
	}
	return false
}

func (f *logsFilter) rejects(entry *types.Log) bool {
	if !f.hasAddress(entry.Address) {
		return true
	}
	for i, topic := range entry.Topics {
		if !f.hasTopic(i, topic) {
			return true
		}
	}
	return false
}

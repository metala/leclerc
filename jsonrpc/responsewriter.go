package jsonrpc

import (
	"encoding/json"
	"io"
)

type ResponseWriter struct {
	io.Writer
}

func NewResponseWriter(w io.Writer) *ResponseWriter {
	return &ResponseWriter{w}
}

func (w *ResponseWriter) WriteJSON(any interface{}) error {
	b, err := json.Marshal(any)
	if err != nil {
		return err
	}
	_, err = w.Write(b)
	return err
}

func (w *ResponseWriter) WriteByte(b byte) error {
	_, err := w.Write([]byte{b})
	return err
}

func (w *ResponseWriter) WriteString(s string) (int, error) {
	return w.Write([]byte(s))
}
func (w *ResponseWriter) WriteRpcError(err *Error) error {
	w.WriteString(`"error":`)
	return w.WriteJSON(err)
}

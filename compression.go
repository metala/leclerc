package leclerc

import (
	"compress/flate"
	"compress/gzip"
	"io"
	"net/http"
	"strings"

	"github.com/dchest/cbrotli"
)

type compressionEncoding int

const (
	EncodingNone compressionEncoding = iota
	EncodingGzip
	EncodingDeflate
	EncodingBrotli
)

func makeHTTPCompressor(w http.ResponseWriter, r *http.Request) io.WriteCloser {
	if w.Header().Get("Vary") == "" {
		w.Header().Set("Vary", "Accept-Encoding")
	}

	encoding := getAcceptableEncoding(r)
	switch encoding {
	case EncodingBrotli:
		w.Header().Set("Content-Encoding", "br")
		return cbrotli.NewWriter(w, cbrotli.WriterOptions{Quality: 3})
	case EncodingGzip:
		w.Header().Set("Content-Encoding", "gzip")
		return gzip.NewWriter(w)
	case EncodingDeflate:
		w.Header().Set("Content-Encoding", "deflate")
		wr, _ := flate.NewWriter(w, -1)
		return wr
	}
	return nopCloser{w}
}

func getAcceptableEncoding(r *http.Request) compressionEncoding {
	encoding := EncodingNone
	accepts := strings.Split(r.Header.Get("accept-encoding"), ",")
	for _, acceptable := range accepts {
		enc := EncodingNone
		acceptable = strings.TrimSpace(acceptable)
		switch acceptable {
		case "br":
			enc = EncodingBrotli
		case "gzip":
			enc = EncodingGzip
		case "deflate":
			enc = EncodingDeflate
		}
		if enc > encoding {
			encoding = enc
		}
	}
	return encoding
}

type nopCloser struct {
	io.Writer
}

func (nopCloser) Close() error { return nil }

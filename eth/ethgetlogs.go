package ethsvc

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/json"
	"fmt"

	"github.com/RoaringBitmap/roaring"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/rpc"
	"github.com/ledgerwatch/erigon-lib/kv"
	"github.com/ledgerwatch/erigon/common/dbutils"
	"github.com/ledgerwatch/erigon/ethdb/bitmapdb"
	"github.com/ledgerwatch/erigon/ethdb/cbor"

	"go.metala.org/leclerc/jsonrpc"
)

type getLogsFilter struct {
	FromBlock rpc.BlockNumber      `json:"fromBlock"`
	ToBlock   rpc.BlockNumber      `json:"toBlock"`
	Address   singleOrArrayAddress `json:"address"`
	Topics    []singleOrArrayHash  `json:"topics"`
}

func (svc *EthService) GetLogs(c context.Context, params []json.RawMessage, w *jsonrpc.ResponseWriter) error {
	if len(params) < 1 {
		return jsonrpc.ErrInvalidParams
	}

	var args getLogsFilter
	err := json.Unmarshal(params[0], &args)
	if err != nil {
		return jsonrpc.ErrInvalidParams
	}

	tx, err := svc.db.BeginRo(c)
	if err != nil {
		return jsonrpc.NewInternalError(err)
	}
	defer tx.Rollback()

	from, err := getBlockNumber(args.FromBlock, tx)
	if err != nil {
		return jsonrpc.NewInternalError(err)
	}
	to, err := getBlockNumber(args.ToBlock, tx)
	if err != nil {
		return jsonrpc.NewInternalError(err)
	}

	addresses := args.Address
	var topics [][]common.Hash
	for _, x := range args.Topics {
		topics = append(topics, x)
	}

	// Do not use jsonrpc.Error here below.
	logs, errc := svc.asyncGetLogs(c, tx, from, to, addresses, topics)
	w.WriteString(`"result":`)
	w.WriteByte('[')
	for first := true; ; {
		select {
		case batch := <-logs:
			svc.writeLogs(w, batch, &first)
		case err = <-errc:
			if err != nil {
				return err
			}

			w.WriteByte(']')
			return nil
		}
	}
}

func (svc *EthService) asyncGetLogs(c context.Context, tx kv.Tx, begin, end uint64, addresses []common.Address, topics [][]common.Hash) (chan []*types.Log, chan error) {
	ch := make(chan []*types.Log)
	errc := make(chan error, 1)

	go func() {
		defer close(errc)
		defer close(ch)

		blockNumbers := roaring.New()
		blockNumbers.AddRange(begin, end+1) // [min,max)

		topicsBitmap, err := getTopicsBitmap(tx, topics, uint32(begin), uint32(end))
		if err != nil {
			errc <- err
			return
		}
		if topicsBitmap != nil {
			if blockNumbers.GetCardinality() == 0 {
				blockNumbers = topicsBitmap
			} else {
				blockNumbers.And(topicsBitmap)
			}
		}

		var addrBitmap *roaring.Bitmap
		for _, addr := range addresses {
			m, err := bitmapdb.Get(tx, kv.LogAddressIndex, addr[:], uint32(begin), uint32(end))
			if err != nil {
				errc <- err
				return
			}
			if addrBitmap == nil {
				addrBitmap = m
			} else {
				addrBitmap = roaring.Or(addrBitmap, m)
			}
		}

		if addrBitmap != nil {
			if blockNumbers.GetCardinality() == 0 {
				blockNumbers = addrBitmap
			} else {
				blockNumbers.And(addrBitmap)
			}
		}

		if blockNumbers.GetCardinality() == 0 {
			// errc <- nil // deferred close does that
			return
		}

		filter := newFilter(addresses, topics)
		iter := blockNumbers.Iterator()
		for iter.HasNext() {
			if err := c.Err(); err != nil {
				errc <- err
				return
			}

			blockNumber := uint64(iter.Next())
			var logIndex uint
			err := tx.ForPrefix(kv.Log, dbutils.EncodeBlockNumber(blockNumber), func(k, v []byte) error {
				var logs []*types.Log
				if err := cbor.Unmarshal(&logs, bytes.NewReader(v)); err != nil {
					return fmt.Errorf("receipt unmarshal failed:  %w", err)
				}
				filtered := make([]*types.Log, 0, len(logs))
				for _, log := range logs {
					log.Index = logIndex
					logIndex++
					if filter.rejects(log) {
						continue
					}

					log.BlockNumber = blockNumber
					txIndex := uint(binary.BigEndian.Uint32(k[8:]))
					log.TxIndex = txIndex

					filtered = append(filtered, log)
				}
				if len(logs) > 0 {
					ch <- filtered
				}
				return nil
			})
			if err != nil {
				errc <- err
				return
			}
		}
	}()

	return ch, errc
}
func (*EthService) writeLogs(w *jsonrpc.ResponseWriter, logs []*types.Log, first *bool) {
	for _, l := range logs {
		if *first {
			*first = false
		} else {
			w.WriteByte(',')
		}

		//
		// b, _ := json.Marshal(l)
		// w.Write(b)

		//
		fmt.Fprintf(w,
			`{"blockNumber":"0x%x","address":"0x%x","data":"0x%x","logIndex":"0x%x",`+
				`"blockHash":"0x%x","transactionHash":"0x%x","transactionIndex":"0x%x","removed":"false",`+
				`"topics":[`,
			l.BlockNumber, l.Address, l.Data, l.Index, l.BlockHash, l.TxHash, l.TxIndex,
		)

		nrTopics := len(l.Topics) / 32
		for j := 0; j < nrTopics; j++ {
			if j != 0 {
				w.WriteByte(',')
			}
			fmt.Fprintf(w, `"0x%x"`, l.Topics[j])
		}
		w.WriteString(`]}`)
	}
}

func getTopicsBitmap(c kv.Tx, topics [][]common.Hash, from, to uint32) (*roaring.Bitmap, error) {
	var result *roaring.Bitmap
	for _, sub := range topics {
		var bm *roaring.Bitmap
		for _, topic := range sub {
			m, err := bitmapdb.Get(c, kv.LogTopicIndex, topic[:], from, to)
			if err != nil {
				return nil, err
			}
			if bm == nil {
				bm = m
				continue
			}
			bm.Or(m)
		}
		if bm == nil {
			continue
		}
		if result == nil {
			result = bm
			continue
		}
		result = roaring.And(bm, result)
	}
	return result, nil
}
